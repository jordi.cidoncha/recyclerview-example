package com.example.recyclerview

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class FilmListFragment: Fragment(R.layout.fragment_film_list) {
    private lateinit var recyclerView: RecyclerView
    private lateinit var fabButton: FloatingActionButton
    private val viewModel: FilmListViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        recyclerView = view.findViewById(R.id.recycler_view)
        fabButton = view.findViewById(R.id.fab_button)

        // Definim com estaran ordenats els elements de la llista (pot ser Linear, Grid o Custom)
        recyclerView.layoutManager = LinearLayoutManager(context)
        // Assignem un Adapter al RecyclerView passant-li per paràmetre la llista d'elements
        recyclerView.adapter = FilmListAdapter(viewModel.getFilms())

        fabButton.setOnClickListener {
            findNavController().navigate(R.id.action_filmList_to_filmDetail)
        }

    }

}