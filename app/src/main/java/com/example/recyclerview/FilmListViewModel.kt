package com.example.recyclerview

import androidx.lifecycle.ViewModel
import kotlin.random.Random

class FilmListViewModel: ViewModel() {
    private var films = mutableListOf<Film>()
    val filmCovers = arrayOf(
        R.drawable.cover1,
        R.drawable.cover2,
        R.drawable.cover3)
    private val filmTypes = arrayOf(
        "Horror",
        "Sci-fi",
        "Action",
        "Comedy",
        "Thriller"
    )

    init {
        for (i in 1..100) {
            films.add(Film("Film #$i",
                Random.nextInt(5),
                Random.nextInt(11),
                filmCovers[Random.nextInt(3)]))
        }
    }

    fun getFilms() = films

    fun addFilm(film: Film) = films.add(film)

    fun updateFilm(filmPosition: Int, film: Film) {
        films[filmPosition] = film
    }
}