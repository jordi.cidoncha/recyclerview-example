package com.example.recyclerview

data class Film  (var title: String, var type: Int, var score: Int, var cover: Int)