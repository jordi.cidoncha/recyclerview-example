package com.example.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView

private val filmTypes = arrayOf(
    "Horror",
    "Sci-fi",
    "Action",
    "Comedy",
    "Thriller"
)

// La classe Adapter serveix per gestionar els items del RecyclerView
class FilmListAdapter(films: List<Film>) : RecyclerView.Adapter<FilmListAdapter.FilmListViewHolder>() {
    private val films = films

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmListViewHolder {
        // Estem creant la vista del layout de l'item
        val view = LayoutInflater.from(parent.context).inflate(R.layout.film_list_item, parent, false)

        // I retornem un FilmListViewHolder que gestionarà aquesta vista
        return FilmListViewHolder(view)
    }

    override fun onBindViewHolder(holder: FilmListViewHolder, position: Int) {
        // Cridarem al holder (FilmListViewHolder) passant-li per
        // paràmetre un item de la llista
        holder.bindData(films[position])

        holder.itemView.setOnClickListener {
            val directions = FilmListFragmentDirections.actionFilmListToFilmDetail(position)
            Navigation.findNavController(it).navigate(directions)
        }
    }

    override fun getItemCount(): Int {
        // Retorno la mida completa de la llista
        return films.size
    }

    // La classe ViewHolder representa una fila concreta del RecyclerView
    // i s'encarrega de dibuixar aquesta fila
    class FilmListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Definim variables de la vista
        private var coverImage: ImageView
        private var titleText: TextView
        private var genreText: TextView
        private var scoreText: TextView

        init {
            // Les inicialitzem en el constructor
            coverImage = itemView.findViewById(R.id.cover_image)
            titleText = itemView.findViewById(R.id.title_text)
            genreText = itemView.findViewById(R.id.genre_text)
            scoreText = itemView.findViewById(R.id.score_text)
        }

        // Funció que ens servirà per assignar la infomarció d'un item de la llista
        // a la vista que el representa
        fun bindData(film: Film) {
            coverImage.setImageResource(film.cover)
            titleText.setText(film.title)
            genreText.setText(filmTypes[film.type])
            scoreText.setText(film.score.toString())

        }

    }
}