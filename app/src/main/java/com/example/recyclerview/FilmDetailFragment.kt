package com.example.recyclerview

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputLayout
import kotlin.random.Random


class FilmDetailFragment : Fragment(R.layout.fragment_film_detail) {
    private lateinit var nameLayout: TextInputLayout
    private lateinit var nameEditText: EditText
    private lateinit var spinner: Spinner
    private lateinit var scoreLayout: TextInputLayout
    private lateinit var scoreEditText: EditText
    private lateinit var filmImageView: ImageView
    private lateinit var addButton: Button
    private var film: Film? = null
    private var filmPosition: Int? = null
    private val viewModel: FilmListViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments?.isEmpty != true) {
            filmPosition = arguments!!.get("filmPosition") as Int
            film = viewModel.getFilms()[filmPosition!!]
        }

        nameLayout = view.findViewById(R.id.name_layout)
        nameEditText = view.findViewById(R.id.name_text)
        spinner = view.findViewById(R.id.type_spinner)
        scoreLayout = view.findViewById(R.id.score_layout)
        scoreEditText = view.findViewById(R.id.score_text)
        filmImageView = view.findViewById(R.id.film_cover)
        addButton = view.findViewById(R.id.add_button)

        if (film != null) {
            addButton.text = getString(R.string.update_button_text)
            nameEditText.setText(film!!.title)
            spinner.setSelection(film!!.type)
            scoreEditText.setText(film!!.score.toString())
            filmImageView.setImageResource(film!!.cover)

        }

        addButton.setOnClickListener {
            if (checkFields()) {
                film = Film(
                    nameEditText.text.toString(),
                    spinner.selectedItemPosition,
                    scoreEditText.text.toString().toInt(),
                    viewModel.filmCovers[Random.nextInt(3)]
                )
                if (addButton.text.equals("Add")) viewModel.addFilm(film!!)
                else viewModel.updateFilm(filmPosition!!, film!!)
                findNavController().navigate(R.id.action_filmDetail_to_filmList)
            }
        }
    }

    private fun checkFields(): Boolean {
        if (nameEditText.text.toString() == "") nameLayout.error = getString(R.string.error_message)
        if (scoreEditText.text.toString() == "") scoreLayout.error = getString(R.string.error_message)
        return nameEditText.text.toString() != "" && scoreEditText.text.toString() != ""
    }
}